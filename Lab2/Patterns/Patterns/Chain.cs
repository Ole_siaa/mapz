﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Patterns
{
    class Chain
    {
        public bool BankTransfer { get; set; }
        
        public bool MoneyTransfer { get; set; }
        
        public bool PayPalTransfer { get; set; }
        public Chain()
        {
            Random rand = new Random();
            BankTransfer = Convert.ToBoolean(rand.Next(2));// запит на активність 
            MoneyTransfer = Convert.ToBoolean(rand.Next(2));
            PayPalTransfer = Convert.ToBoolean(rand.Next(2));
        }
    }
    abstract class PaymentHandler
    {
        public PaymentHandler Successor { get; set; }
        public abstract void Handle(Chain receiver);
    }

    class BankPaymentHandler : PaymentHandler
    {
        public override void Handle(Chain receiver)
        {
            if (receiver.BankTransfer == true)
                System.Windows.Forms.MessageBox.Show("Активовано банківський переказ");
            else if (Successor != null)
                Successor.Handle(receiver);
        }
    }

    class PayPalPaymentHandler : PaymentHandler
    {
        public override void Handle(Chain receiver)
        {
            if (receiver.PayPalTransfer == true)
                System.Windows.Forms.MessageBox.Show("Аквивовано переказ через PayPal");
            else if (Successor != null)
                Successor.Handle(receiver);
        }
    }
    
    class MoneyPaymentHandler : PaymentHandler
    {
        public override void Handle(Chain receiver)
        {
            if (receiver.MoneyTransfer == true)
                System.Windows.Forms.MessageBox.Show("Активована система грошових переказів");
            else if (Successor != null)
                Successor.Handle(receiver);
            else 
                System.Windows.Forms.MessageBox.Show("Системи переказів тимчасово не працюють :(");
        }
    }
}
