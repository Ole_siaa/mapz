﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Patterns
{
    class Facade
    {
        EditText edittext;
        Dress dress;
        Shoes shoes;
        MainForm mainform;
        public Facade(MainForm mainform)
        {
            this.mainform = mainform;
            edittext = new EditText();
            dress = new Dress();
            shoes = new Shoes();
        }
        public void setcloser()
        {
            edittext.start();
            mainform.setterdress = dress.setdress();
            mainform.setshoes = shoes.setshoes();
            edittext.stop();
        }
    }

    class EditText
    {
        public void start()
        {
            System.Windows.Forms.MessageBox.Show("Мммм...Одягни, одягни мене повністю");
        }
        public void stop()
        {
            System.Windows.Forms.MessageBox.Show("Одягнув... :(");
        }
    }

    class Dress
    {
        public System.Drawing.Bitmap setdress()
        {
            DressSelector fm2 = new DressSelector();
            fm2.ShowDialog();
            System.Drawing.Bitmap image = fm2.imageGETSET;
            return image;
        }
    }
    class Shoes
    {
        public System.Drawing.Bitmap setshoes()
        {
            Shoesselector fm2 = new Shoesselector();
            fm2.ShowDialog();
            System.Drawing.Bitmap image = fm2.imageGETSET;
            return image;
        }
    }
}
