﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Patterns
{
    class SingletonUser
    {
        private static SingletonUser instance;
        private SingletonUser() { }
        public static SingletonUser getinstance()
        {
            if (instance == null)
            {
                instance = new SingletonUser();
                SingletonLoger log = SingletonLoger.getinstance();
                log.writelog($"Об'єкт дівчини створено в {DateTime.Now}");
            }
            else
            {
                System.Windows.Forms.MessageBox.Show("Об'єкт вже створено");
            }
            return instance;
        }
        public System.Drawing.Bitmap image()
        {
            return Properties.Resources._girl_1;
        }
    }
}
