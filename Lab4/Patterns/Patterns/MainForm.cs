﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Patterns
{
    public partial class MainForm : Form
    {
        public MainForm()
        {
            InitializeComponent();
        }
        public System.Drawing.Bitmap setterdress
        {
            set{ pictureBox2.Image = value;
                pictureBox2.SizeMode = PictureBoxSizeMode.Zoom;
            }
        }

        public System.Drawing.Bitmap setshoes
        {
            set
            {
                pictureBox3.Image = value;
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            SingletonUser singlUser = SingletonUser.getinstance();
            pictureBox1.Image = singlUser.image();
            pictureBox1.SizeMode = PictureBoxSizeMode.Zoom;
            button2.Visible = true;
        }

        private void MainForm_FormClosed(object sender, FormClosedEventArgs e)
        {
            Application.Exit();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            Facade facade = new Facade(this);
            facade.setcloser();
        }

        private void button3_Click(object sender, EventArgs e)
        {
            PriceList priceform = new PriceList();
            priceform.ShowDialog();    
        }

        private void button4_Click(object sender, EventArgs e)
        {
            Chat chat = new Chat();
            chat.ShowDialog();
        }

        private void button5_Click(object sender, EventArgs e)
        {
            Chain receiver = new Chain();

            PaymentHandler bankPaymentHandler = new BankPaymentHandler();
            PaymentHandler moneyPaymentHadler = new MoneyPaymentHandler();
            PaymentHandler paypalPaymentHandler = new PayPalPaymentHandler();

            bankPaymentHandler.Successor = paypalPaymentHandler;
            paypalPaymentHandler.Successor = moneyPaymentHadler;

            bankPaymentHandler.Handle(receiver);
        }
    }
}
