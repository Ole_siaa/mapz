﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Patterns
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            SingletonLoger logs = SingletonLoger.getinstance();            
            logs.writelog("Захід в гру "+DateTime.Now+"");
            MainForm mainform = new MainForm();
            Level level;
            switch (comboBox1.SelectedIndex)
            {
                case 0:
                    level = new Level(new Level1_Factory());
                    break;
                case 1:
                    level = new Level(new Level2_Factory());
                    break;
                case 2:
                    level = new Level(new Level3_Factory());
                    break;
                default:
                    level = new Level(new Level1_Factory());
                    break;
            }
            mainform.BackColor = level.Hit();
            mainform.Show();
            level.Show();
            

            //this.Hide();
        }
    }
    abstract class LevelFactory
    {
        public abstract Style CreateStale();
        public abstract MyMessage CreareMessage();

    }
    class Level1_Factory : LevelFactory
    {
        public override Style CreateStale()
        {
            return new Level1_Stale();
        }
        public override MyMessage CreareMessage()
        {
            return new Level1_MyMessage();
        }
    }
    class Level2_Factory : LevelFactory
    {
        public override Style CreateStale()
        {
            return new Level2_Stale();
        }
        public override MyMessage CreareMessage()
        {
            return new Level2_MyMessage();
        }
    }

    class Level3_Factory : LevelFactory
    {
        public override Style CreateStale()
        {
            return new Level3_Stale();
        }
        public override MyMessage CreareMessage()
        {
            return new Level3_MyMessage();
        }
    }

    class Level
    {
        private Style level;
        private MyMessage levelmes;
        public Level(LevelFactory factory)
        {
            level = factory.CreateStale();
            levelmes = factory.CreareMessage();
        }
        public Color Hit()
        {
            return level.Hit();
        }
        public void Show()
        {
            levelmes.Show();
        }
        
    }
    abstract class Style
    {
        public abstract Color Hit();
    }
        
    class Level1_Stale : Style
    {
        public override Color Hit()
        {
            return Color.Pink;
        }

    }
    class Level2_Stale : Style
    {
        public override Color Hit()
        {
            return Color.PeachPuff;
        }
    }
    class Level3_Stale : Style
    {
        public override Color Hit()
        {
            return Color.Blue;
        }
    }

    abstract class MyMessage
    {
        public abstract void Show();
    }
    class Level1_MyMessage : MyMessage
    {
        public override void Show()
        {
            MessageBox.Show("Level 1");
        }

    }
    class Level2_MyMessage : MyMessage
    {
        public override void Show()
        {
            MessageBox.Show("Level 2");
        }

    }
    class Level3_MyMessage : MyMessage
    {
        public override void Show()
        {
            MessageBox.Show("Level 3");
        }

    }

}
