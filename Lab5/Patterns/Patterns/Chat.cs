﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Patterns
{
    public partial class Chat : Form
    {
        Originator originator;
        Caretaker caretaker;

        public Chat()
        {
            InitializeComponent();
        }
        public void writetext(string message)
        {
            textBox1.AppendText(message+"\r\n");
        }

        private void button1_Click(object sender, EventArgs e)
        {
            originator = new Originator("Привіт)", this);
            caretaker = new Caretaker(originator,this);

        }

        private void button2_Click(object sender, EventArgs e)
        {
            if (originator!=null && textBox2.Text!="")
            {
                caretaker.Backup();
                originator.DoSomething(textBox2.Text);
                textBox2.Text = "";
                button4.Visible = true;
            }
        }

        private void button3_Click(object sender, EventArgs e)
        {
            if(originator!=null)
            {
                writetext("");
                caretaker.ShowHistory();
            }
        }

        private void button4_Click(object sender, EventArgs e)
        {
            writetext("\nClient: Now, let's rollback!\n");
            caretaker.Undo();
        }
    }
    class Originator
    {
        private string _state;
        Chat chat; 
        public Originator(string state,Chat chat)
        {
            this._state = state;
            chat.writetext("Originator: My initial state is: " + state);
            this.chat = chat;
        }
        public void DoSomething(string mess)
        {
            chat.writetext("Originator: I'm doing something important.");
            this._state = mess;
            chat.writetext($"Originator: and my state has changed to: {_state}");
        }


        
        public IMemento Save()
        {
            return new ConcreteMemento(this._state);
        }

        
        public void Restore(IMemento memento)
        {
            if (!(memento is ConcreteMemento))
            {
                throw new Exception("Unknown memento class " + memento.ToString());
            }

            this._state = memento.GetState();
            Console.Write($"Originator: My state has changed to: {_state}");
        }
    }
    public interface IMemento
    {
        string GetName();

        string GetState();

        DateTime GetDate();
    }
    class ConcreteMemento : IMemento
    {
        private string _state;

        private DateTime _date;

        public ConcreteMemento(string state)
        {
            this._state = state;
            this._date = DateTime.Now;
        }
        public string GetState()
        {
            return this._state;
        }
        public string GetName()
        {
            return $"{this._date} / ({this._state})...";
        }

        public DateTime GetDate()
        {
            return this._date;
        }
    }
    class Caretaker
    {
        private List<IMemento> _mementos = new List<IMemento>();

        private Originator _originator = null;
        Chat chat;
        public Caretaker(Originator originator,Chat _chat)
        {
            chat = _chat;
            this._originator = originator;
        }

        public void Backup()
        {
            chat.writetext("\nCaretaker: Saving Originator's state...");
            this._mementos.Add(this._originator.Save());
        }

        public void Undo()
        {
            if (this._mementos.Count == 0)
            {
                return;
            }

            var memento = this._mementos.Last();
            this._mementos.Remove(memento);

            chat.writetext("Caretaker: Restoring state to: " + memento.GetName());

            try
            {
                this._originator.Restore(memento);
            }
            catch (Exception)
            {
                this.Undo();
            }
        }

        public void ShowHistory()
        {
            chat.writetext("Caretaker: Here's the list of mementos:");

            foreach (var memento in this._mementos)
            {
                chat.writetext(memento.GetName());
            }
        }
    }
}
