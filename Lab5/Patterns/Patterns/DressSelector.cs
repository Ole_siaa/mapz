﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Patterns
{
    public partial class DressSelector : Form
    {
        public DressSelector()
        {
            InitializeComponent();
        }
        private System.Drawing.Bitmap image;
        public System.Drawing.Bitmap imageGETSET
        {
            get
            {
                return image;
            }
            set
            {
                image = value;
            }
        }

        private void pictureBox1_Click(object sender, EventArgs e)
        {
            imageGETSET = Properties.Resources.dress;
            this.Close();
        }

        private void pictureBox2_Click(object sender, EventArgs e)
        {
            imageGETSET = Properties.Resources.Yellowdress1;
            this.Close();
        }
    }
}
