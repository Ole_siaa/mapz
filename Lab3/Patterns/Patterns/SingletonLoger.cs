﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Patterns
{
    class SingletonLoger
    {
        private static SingletonLoger instance;
        private static object syncRoot = new Object();
        private SingletonLoger() { }
        public static SingletonLoger getinstance()
        {
            if(instance==null)
            {
                lock (syncRoot)
                {
                    if (instance == null)
                        instance = new SingletonLoger();
                }
            }
            return instance;
        }
        public void writelog(string massage)
        {
            string path = AppDomain.CurrentDomain.BaseDirectory + "\\Logs";
            if (!Directory.Exists(path))  // name space(using System.IO;)
            {
                Directory.CreateDirectory(path);
            }
            string filepath = AppDomain.CurrentDomain.BaseDirectory + "\\Logs\\Log_" + DateTime.Now.Date.ToShortDateString().Replace('/', '_') + ".txt";
            if (!File.Exists(filepath))
            {
                // Create a file to write to.   
                using (StreamWriter sw = File.CreateText(filepath))
                {
                    sw.WriteLine(massage);
                }
            }
            else
            {
                using (StreamWriter sw = File.AppendText(filepath))
                {
                    sw.WriteLine(massage);
                }
            }
        }
    }
}
