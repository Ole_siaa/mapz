﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Patterns
{
    public partial class PriceList : Form
    {
        public PriceList()
        {
            InitializeComponent();
        }

        private void PriceList_Load(object sender, EventArgs e)
        {
            Look look1 = new SportLook();
            look1 = new BagLook(look1); // итальянская пицца с томатами
            label4.Text = look1.Name;
            label4.MaximumSize = new Size(120, 70);
            label1.Text = "Ціна: "+ look1.GetCost();

            Look look2 = new SportLook();
            look2 = new OuterLook(look2);// итальянская пиццы с сыром
            label5.Text = look2.Name;
            label5.MaximumSize = new Size(120, 70);
            label2.Text = "Ціна: " + look2.GetCost();

            Look look3 = new NightLook();
            look3 = new BagLook(look3);
            look3 = new OuterLook(look3);// болгарская пиццы с томатами и сыром
            label6.Text = look3.Name;
            label6.MaximumSize = new Size(120, 100);
            label3.Text = "Ціна: " + look3.GetCost();

            Console.ReadLine();
        }
    }
    abstract class Look
    {
        public Look(string n)
        {
            this.Name = n;
        }
        public string Name { get; protected set; }
        public abstract int GetCost();
        
    }

    class SportLook : Look
    {
        public SportLook() : base("Спортивний стиль")
        { }
        public override int GetCost()
        {
            return 100;
        }
    }
    class NightLook : Look
    {
        public NightLook()
            : base("Вечірній стиль")
        { }
        public override int GetCost()
        {
            return 80;
        }
    }

    abstract class LookDecorator : Look
    {
        protected Look look;
        public LookDecorator(string n, Look pizza) : base(n)
        {
            this.look = pizza;
        }
    }

    class BagLook : LookDecorator
    {
        public BagLook(Look p)
            : base(p.Name + " з сумкою", p)
        { }

        public override int GetCost()
        {
            return look.GetCost() + 30;
        }
    }

    class OuterLook : LookDecorator
    {
        public OuterLook(Look p)
            : base(p.Name + " з верхнім одягом", p)
        { }

        public override int GetCost()
        {
            return look.GetCost() + 50;
        }
    }
}
