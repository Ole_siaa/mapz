﻿using System;
using System.Collections.Generic;
using System.Data;
using Interpreter.Exps;
using Interpreter.Exps.Factors;
using Interpreter.Exps.Kwords;
using Interpreter.Exps.Ops;
using Interpreter.Tokens;

namespace Interpreter
{
    class Parser
    {
        private Queue<Token> tknQueue;

        public Exp Parse(Queue<Token> tokens)
        {
            tknQueue = tokens;
            var stmts = new List<Exp>();
            while (tknQueue.Count != 0) 
                stmts.Add(GetStmt());

            return new BlockExp(stmts);
        }

        private void EatToken(TypeOfToken type)
        {
            if (tknQueue.Count == 0) throw new Exception("Unexpected end of file");
            if (tknQueue.Peek().Type == type)
            {
                tknQueue.Dequeue();
                return;
            }

            throw new Exception("Unexpected token");
        }

        private Exp GetStmt()
        {
            Exp stmt;
            if (tknQueue.Peek().Type == TypeOfToken.LFigure)
                return GetBlock();
            if (tknQueue.Peek().Type == TypeOfToken.Cycle)
                return GetCycle();
            if (tknQueue.Peek().Type == TypeOfToken.Check)
                return GetIf();
            if (tknQueue.Peek().Type == TypeOfToken.Var)
            {
                stmt = GetAssign();
                EatToken(TypeOfToken.OpEnd);
                return stmt;
            }

            if (tknQueue.Peek().Type == TypeOfToken.RFigure)
                return null;
            if (tknQueue.Peek().Type == TypeOfToken.OpEnd)
            {
                tknQueue.Dequeue();
                return null;
            }

            if (tknQueue.Peek().Type == TypeOfToken.Int || tknQueue.Peek().Type == TypeOfToken.Str ||
                tknQueue.Peek().Type == TypeOfToken.Bool)
            {
                var exp = GetExp();
                EatToken(TypeOfToken.OpEnd);
                return exp;
            }

            if (tknQueue.Peek().Type == TypeOfToken.Go || tknQueue.Peek().Type == TypeOfToken.Write ||
                tknQueue.Peek().Type == TypeOfToken.Look)
            {
                stmt = GetKword();
                EatToken(TypeOfToken.OpEnd);
                return stmt;
            }

            throw new Exception("Unknown stmt");
        }

        private ObjectExp GetObj()
        {
            var properties = new Dictionary<string, Exp>();

            EatToken(TypeOfToken.LFigure);

            var fTimeIn = true;

            do
            {
                if (!fTimeIn)
                    EatToken(TypeOfToken.Coma);

                if (tknQueue.Peek().Type != TypeOfToken.Var)
                    throw new SyntaxErrorException("Unknown expression");

                var propName = tknQueue.Dequeue().Val;

                EatToken(TypeOfToken.Assign);

                var propValue = GetExp();

                properties.Add(propName, propValue);
                fTimeIn = false;
            } while (tknQueue.Peek().Type == TypeOfToken.Coma);

            EatToken(TypeOfToken.RFigure);

            return new ObjectExp(new ObjectInstance(properties));
        }

        private BlockExp GetBlock()
        {
            var expressions = new List<Exp>();

            EatToken(TypeOfToken.LFigure);

            while (tknQueue.Peek().Type != TypeOfToken.RFigure)
            {
                var expression = GetStmt();
                if (expression == null) continue;
                expressions.Add(expression);
            }

            EatToken(TypeOfToken.RFigure);

            return new BlockExp(expressions);
        }

        private CycleExp GetCycle()
        {
            EatToken(TypeOfToken.Cycle);

            var condition = GetExp();

            EatToken(TypeOfToken.Do);

            var body = GetStmt();

            return new CycleExp(condition, body);
        }

        private ConditionalExp GetIf()
        {
            EatToken(TypeOfToken.Check);

            var condition = GetExp();

            EatToken(TypeOfToken.Do);

            var trueBody = GetStmt();

            return new ConditionalExp(condition, trueBody);
        }

        private AssignExp GetAssign()
        {
            var variable = tknQueue.Dequeue();

            EatToken(TypeOfToken.Assign);

            var expression = tknQueue.Peek().Type == TypeOfToken.LFigure ? GetObj() : GetExp();

            return new AssignExp(variable.Val, expression);
        }

        private Exp GetKword()
        {
            var func = tknQueue.Dequeue();
            if (func.Type == TypeOfToken.Look)
            {
                var arg = GetExp();
                return new LookExp(arg);
            }

            if (func.Type == TypeOfToken.Go)
            {
                var arg = GetExp();
                return new GoExp(arg);
            }

            if (func.Type == TypeOfToken.Write)
            {
                var arg = GetExp();
                EatToken(TypeOfToken.Coma);
                var arg2 = GetExp();
                return new WriteExp(arg, arg2);
            }

            throw new Exception("Unknown stmt");
        }

        private Exp GetExp()
        {
            var left = GetLowPr();
            if (tknQueue.Count == 0) return left;

            if (tknQueue.Peek().Type != TypeOfToken.Equal) return left;

            EatToken(TypeOfToken.Equal);
            var right = GetLowPr();
            return new EqualExp(left, right);
        }

        private Exp GetLowPr()
        {
            var left = GetHighPr();
            if (tknQueue.Count == 0) return left;
            while (tknQueue.Peek().Type == TypeOfToken.Plus || tknQueue.Peek().Type == TypeOfToken.Minus)
            {
                var currTkn = tknQueue.Peek();

                if (currTkn.Type == TypeOfToken.Plus)
                {
                    EatToken(TypeOfToken.Plus);
                    left = new AddExp(left, GetHighPr());
                    continue;
                }

                if (currTkn.Type == TypeOfToken.Minus)
                {
                    EatToken(TypeOfToken.Minus);
                    left = new SubtractExp(left, GetHighPr());
                }
            }

            return left;
        }

        private Exp GetHighPr()
        {
            var node = GetFact();
            if (tknQueue.Count == 0) return node;
            while (tknQueue.Peek().Type == TypeOfToken.Multiply || tknQueue.Peek().Type == TypeOfToken.Divide)
            {
                var currentToken = tknQueue.Peek();

                if (currentToken.Type == TypeOfToken.Multiply)
                {
                    EatToken(TypeOfToken.Multiply);
                    node = new MultiplyExp(node, GetFact());
                    continue;
                }

                if (currentToken.Type == TypeOfToken.Divide)
                {
                    EatToken(TypeOfToken.Divide);
                    node = new DivideExp(node, GetFact());
                }
            }

            return node;
        }

        private Exp GetFact()
        {
            var token = tknQueue.Dequeue();
            if (token.Type == TypeOfToken.Var)
            {
                if (tknQueue.Peek().Type == TypeOfToken.Point)
                {
                    EatToken(TypeOfToken.Point);
                    var propName = tknQueue.Dequeue();
                    if (propName.Type != TypeOfToken.Var) throw new Exception("Unknown expression");
                    return new PointExp(new VarExp(token.Val), propName.Val);
                }

                return new VarExp(token.Val);
            }

            if (token.Type == TypeOfToken.Int)
            {
                var val = int.Parse(token.Val);
                return new IntExp(val);
            }

            if (token.Type == TypeOfToken.Str)
            {
                var val = token.Val.Trim('"');
                return new StringExp(val);
            }

            if (token.Type == TypeOfToken.Bool)
            {
                var val = bool.Parse(token.Val);
                return new BoolExp(val);
            }

            if (token.Type == TypeOfToken.LBracket)
            {
                var node = GetExp();
                EatToken(TypeOfToken.RBracket);
                return node;
            }

            throw new Exception("Unknown operation");
        }
    };
}
