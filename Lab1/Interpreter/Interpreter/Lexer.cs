﻿using System;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using Interpreter.Tokens;

namespace Interpreter
{
    class Lexer
    {
        DictionaryOfTokens tokenDict = new DictionaryOfTokens();
        
        public Queue<Token> Lex(string programText)
        {
            Queue<Token> tokens = new Queue<Token>();
            var remainingText = programText;

            while (remainingText != "")
            {
                var rexps = tokenDict.regexp;
                bool matched = false;
                for (int i = 0; i < rexps.Count; ++i)
                {
                    var match = Regex.Match(remainingText, rexps[i]);

                    if (!string.IsNullOrEmpty(match.Value))
                        matched = true;

                    if (matched)
                    {
                        remainingText = remainingText.Substring(match.Value.Length);
                        var tType = tokenDict.types[i];
                        if(tType != TypeOfToken.None)
                            tokens.Enqueue(new Token(tType, match.Value));
                        break;
                    }
                }

                if(!matched) throw new Exception("Unknown expression");

                
            }
            return tokens;
        }
    }
}
