﻿using System;
using System.IO;

namespace Interpreter
{
    class Program
    {
        static void Main()
        {
            var lexer = new Lexer();
            var parser = new Parser();
            var program = parser.Parse(lexer.Lex(File.ReadAllText(@"D:\mapz1\Variant4\Interpreter\Interpreter\MyProgram.txt")));
            Console.WriteLine("AST:\n");
            Console.WriteLine(program.DrawTree());
            Console.WriteLine("AST optimized:\n");
            program = program.Optimize();
            Console.WriteLine(program.DrawTree());
            using (var context = new Ctxt())
            {
                program.Exec(context);
                Console.ReadKey();
            }
        }
    }
}
