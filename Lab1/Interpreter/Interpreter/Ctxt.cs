﻿using System;
using System.Collections.Generic;
using System.Threading;
using System.Windows.Forms;
using Interpreter.Browser;

namespace Interpreter
{
    class Ctxt: IDisposable
    {
        public BrowserSync browser;
        private readonly Thread thread;

        public Dictionary<string, object> variables;

        public event EventHandler Close;

        public Ctxt()
        {
            variables = new Dictionary<string, object>();

            Barrier barrier = new Barrier(2);
            thread = new Thread(() =>
            {
                try
                {
                    var browserForm = new BrowserForm();
                    browser = new BrowserSync();
                    browserForm.Browser = browser;
                    browserForm.barrier = barrier;
                    Close += browserForm.Close;

                    Application.Run(browserForm);
                }
                catch (ThreadAbortException)
                {

                }
            });
            
            thread.SetApartmentState(ApartmentState.STA);
            thread.Start();
            barrier.SignalAndWait();
            barrier.Dispose();
        }

        public void Dispose()
        {
            if (thread.IsAlive)
            {
                Close?.Invoke(this, new EventArgs());
                thread.Abort();
            }
        }
    }
}
