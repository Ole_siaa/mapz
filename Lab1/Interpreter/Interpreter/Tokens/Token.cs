﻿namespace Interpreter.Tokens
{
    class Token
    {
        public TypeOfToken Type { get; }
        public string Val { get; }

        public Token(TypeOfToken type, string val)
        {
            Type = type;
            Val = val;
        }
    }
}
