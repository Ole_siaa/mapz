﻿using System.Collections.Generic;

namespace Interpreter.Tokens
{
    class DictionaryOfTokens
    {
        public List<string> regexp;
        public List<TypeOfToken> types;

        public DictionaryOfTokens()
        {
            regexp = new List<string>
            {
                @"^\(", @"^\)", @"^{", @"^}",
                @"^->", @"^\d+", @"^look", @"^cycle",
                @"^write", @"^go", @"^check", @"^(?:yes|no)",
                @"^~", @"^do", @"^""[^""]*""", @"^,", 
                @"^\+", @"^\$", @"^-", @"^%",
                @"^\.", @"^[a-zA-Z_][a-zA-Z_0-9]*", @"^!", @"^\s+"
            };

            types = new List<TypeOfToken>
            {
                TypeOfToken.LBracket, TypeOfToken.RBracket, TypeOfToken.LFigure, TypeOfToken.RFigure,
                TypeOfToken.Assign, TypeOfToken.Int, TypeOfToken.Look, TypeOfToken.Cycle,
                TypeOfToken.Write, TypeOfToken.Go, TypeOfToken.Check, TypeOfToken.Bool,
                TypeOfToken.Equal, TypeOfToken.Do, TypeOfToken.Str, TypeOfToken.Coma,
                TypeOfToken.Plus, TypeOfToken.Multiply, TypeOfToken.Minus, TypeOfToken.Divide,
                TypeOfToken.Point, TypeOfToken.Var, TypeOfToken.OpEnd, TypeOfToken.None
            };
        }
    }
}
