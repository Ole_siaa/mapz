﻿using System.Text;

namespace Interpreter.Exps.Factors
{
    class BoolExp : Exp
    {
        public bool value;

        public BoolExp(bool value)
        {
            this.value = value;
        }

        public object Exec(Ctxt ctxt)
        {
            return value;
        }

        public Exp Optimize()
        {
            return this;
        }

        public string DrawTree(int lvl = 0)
        {
            var res = "";
            for (int i = 0; i < lvl; ++i)
            {
                res += "|\t";
            }
            res += "Bool: " + value + '\n';
            return res;
        }
    }
}
