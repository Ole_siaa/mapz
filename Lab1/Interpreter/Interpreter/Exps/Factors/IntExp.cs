﻿using System.Text;

namespace Interpreter.Exps.Factors
{
    class IntExp : Exp
    {
        public int value;

        public IntExp(int value)
        {
            this.value = value;
        }

        public object Exec(Ctxt ctxt)
        {
            return value;
        }

        public Exp Optimize()
        {
            return this;
        }

        public string DrawTree(int lvl = 0)
        {
            var res = "";
            for (int i = 0; i < lvl; ++i)
            {
                res += "|\t";
            }
            res += "Integer: " + value + '\n';
            return res;
        }

    }
}
