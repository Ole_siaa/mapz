﻿using System.Collections.Generic;

namespace Interpreter.Exps.Factors
{
    class ObjectInstance
    {
        public ObjectInstance(IDictionary<string, Exp> properties)
        {
            Properties = properties;
        }

        public IDictionary<string, Exp> Properties { get; }
    }
}
