﻿using System;
using System.Text;

namespace Interpreter.Exps.Factors
{
    class VarExp : Exp
    {
        public string name;

        public VarExp(string name)
        {
            this.name = name;
        }

        public object Exec(Ctxt ctxt)
        {
            if (ctxt.variables.ContainsKey(name) && ctxt.variables[name] != null)
            {
                return ctxt.variables[name];
            }

            throw new ArgumentNullException("Non-declared variable");
        }

        public Exp Optimize()
        {
            return this;
        }

        public string DrawTree(int lvl = 0)
        {
            var res = "";
            for (int i = 0; i < lvl; ++i)
            {
                res += "|\t";
            }
            res += "Variable: " + name + "\n";
            return res;
        }
    }
}
