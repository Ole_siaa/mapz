﻿using System.Linq;
using System.Text;

namespace Interpreter.Exps.Factors
{
    class ObjectExp: Exp
    {
        public ObjectInstance value;

        public ObjectExp(ObjectInstance value)
        {
            this.value = value;
        }

        public object Exec(Ctxt ctxt)
        {
            return value;
        }

        public Exp Optimize()
        {
            var keys = value.Properties.Keys.ToArray();
            foreach (var key in keys)
            {
                value.Properties[key] = value.Properties[key].Optimize();
            }

            return this;
        }

        public string DrawTree(int lvl = 0)
        {
            var res = "";
            for (int i = 0; i < lvl; ++i)
            {
                res += "|\t";
            }
            res += "Object" + ":" + '\n';
            foreach (var property in value.Properties)
            {
                for (int i = 0; i < lvl + 1; ++i)
                {
                    res += "|\t";
                }
                res += property.Key + ":" + '\n';
                res += (property.Value.DrawTree(lvl + 2));
            }
            return res; //+= "Object";
        }
    }
}
