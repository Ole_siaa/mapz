﻿using System.Text;

namespace Interpreter.Exps.Factors
{
    class StringExp : Exp
    {
        public string value;

        public StringExp(string value)
        {
            this.value = value;
        }

        public object Exec(Ctxt ctxt)
        {
            return value;
        }

        public Exp Optimize()
        {
            return this;
        }

        public string DrawTree(int lvl = 0)
        {
            var res = "";
            for (int i = 0; i < lvl; ++i)
            {
                res += "|\t";
            }
            res += "String: " + value + '\n';
            return res;
        }
    }
}
