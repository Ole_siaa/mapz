﻿using System.Text;

namespace Interpreter.Exps.Kwords
{
    class LookExp : Exp
    {
        public Exp link;

        private delegate void NavigateHandler(string url);

        public LookExp(Exp link)
        {
            this.link = link;
        }

        public object Exec(Ctxt ctxt)
        {
            var urlRes = (string)link.Exec(ctxt);
            if (ctxt.browser.InvokeRequired)
            {
                ctxt.browser.Invoke(new NavigateHandler(ctxt.browser.NavigateAndWait), (urlRes));
            }
            else
            {
                ctxt.browser.NavigateAndWait(urlRes);
            }

            ctxt.browser.WaitIfPageLoading(); // wait redirection
            return null;
        }

        public Exp Optimize()
        {
            link = link.Optimize();
            return this;
        }

        public string DrawTree(int lvl = 0)
        {
            var res = "";
            for (int i = 0; i < lvl; ++i)
            {
                res += "|\t";
            }
            res += "Look" + ":" + '\n';
            res += (link.DrawTree(lvl + 1));
     //       res += "Look";
            return res;
        }
    }
}
