﻿using System.Text;
using System.Windows.Forms;

namespace Interpreter.Exps.Kwords
{
    class WriteExp : Exp
    {
        public Exp to;
        public Exp text;

        private delegate HtmlElement ElHndlr(string to);

        public WriteExp(Exp to, Exp text)
        {
            this.to = to;
            this.text = text;
        }

        public object Exec(Ctxt ctxt)
        {
            var toRes = (string) to.Exec(ctxt);
            var textRes = text.Exec(ctxt);

            HtmlElement el;
            if (ctxt.browser.InvokeRequired)
            {
                el = (HtmlElement)ctxt.browser.Invoke(new ElHndlr(ctxt.browser.FindTarget), toRes);
            }
            else
            {
                el = ctxt.browser.FindTarget(toRes);
            }

            if (el != null)
            {
                var newVal = el.GetAttribute("value") + textRes;
                el.SetAttribute("value", newVal);
            }

            return null;
        }

        public Exp Optimize()
        {
            to = to.Optimize();
            text = text.Optimize();
            return this;
        }

        public string DrawTree(int lvl = 0)
        {
            var res = "";
            for (int i = 0; i < lvl; ++i)
            {
                res += "|\t";
            }
            res += "Write" + ":" + '\n';
            res += (to.DrawTree(lvl + 1));
            res += (text.DrawTree(lvl + 1));
     //       res += "Write";
            return res;
        }
    }
}
