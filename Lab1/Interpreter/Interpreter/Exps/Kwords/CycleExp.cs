﻿using System.Text;
using Interpreter.Exps.Factors;

namespace Interpreter.Exps.Kwords
{
    class CycleExp : Exp
    {
        public Exp cond;
        public Exp exp;

        public CycleExp(Exp cond, Exp exp)
        {
            this.exp = exp;
            this.cond = cond;
        }

        public object Exec(Ctxt ctxt)
        {
            while ((bool) cond.Exec(ctxt))
            {
                if(exp != null)
                    exp.Exec(ctxt);
            }

            return null;
        }

        public Exp Optimize()
        {
            cond = cond.Optimize();
            if (exp != null)
                exp = exp.Optimize();

            if (!(cond is BoolExp boolExpression)) return this;
            return boolExpression.value ? this : null;

        }

        public string DrawTree(int lvl = 0)
        {
            var res = "";
            for (int i = 0; i < lvl; ++i)
            {
                res += "|\t";
            }
            res += "Cycle" + ":" + '\n';
            res += (cond.DrawTree(lvl + 1));
            res += (exp?.DrawTree(lvl + 1));
      //      res += "Cycle";
            return res;
        }
    }
}
