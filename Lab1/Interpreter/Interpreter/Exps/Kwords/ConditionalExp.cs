﻿using System.Text;
using Interpreter.Exps.Factors;

namespace Interpreter.Exps.Kwords
{
    class ConditionalExp : Exp
    {
        public Exp cond;
        public Exp exp;

        public ConditionalExp(Exp cond, Exp exp)
        {
            this.cond = cond;
            this.exp = exp;
        }

        public object Exec(Ctxt ctxt)
        {
            if ((bool)cond.Exec(ctxt)) exp.Exec(ctxt);
            return null;
        }

        public Exp Optimize()
        {
            cond = cond.Optimize();
            exp = exp.Optimize();

            if (!(cond is BoolExp boolExpression)) return this;
            return boolExpression.value ? exp : null;

        }

        public string DrawTree(int lvl = 0)
        {
            var res = "";
            for (int i = 0; i < lvl; ++i)
            {
                res += "|\t";
            }
            res += "Conditional" + ":" + '\n';
            res += (cond.DrawTree(lvl + 1));
            res += (exp.DrawTree(lvl + 1));
        //    res += "Conditional";
            return res;
        }
    }
}
