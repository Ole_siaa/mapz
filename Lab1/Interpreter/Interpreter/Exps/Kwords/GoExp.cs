﻿using System.Text;
using System.Windows.Forms;

namespace Interpreter.Exps.Kwords
{
    class GoExp : Exp
    {

        public Exp to;

        private delegate HtmlElement ElHndlr(string to);

        public GoExp(Exp to)
        {
            this.to = to;
        }

        public object Exec(Ctxt ctxt)
        {
            var toRes = (string)to.Exec(ctxt);

            HtmlElement el;

            if (ctxt.browser.InvokeRequired)
            {
                el = (HtmlElement)ctxt.browser.Invoke(new ElHndlr(ctxt.browser.FindTarget), toRes);
            }
            else
            {
                el = ctxt.browser.FindTarget(toRes);
            }
            if (el != null)
            {
                el.InvokeMember("click");
                ctxt.browser.WaitIfPageLoading();
            }

            return null;
        }

        public Exp Optimize()
        {
            to = to.Optimize();

            return this;
        }

        public string DrawTree(int lvl = 0)
        {
            var res = "";
            for (int i = 0; i < lvl; ++i)
            {
                res += "|\t";
            }
            res += "Go" + ":" + '\n';
            res += (to.DrawTree(lvl + 1));
        //    res += "Go";
            return res;
        }
    }
}
