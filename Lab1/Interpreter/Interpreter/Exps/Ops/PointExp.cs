﻿using System;
using System.Text;
using Interpreter.Exps.Factors;

namespace Interpreter.Exps.Ops
{
    class PointExp: Exp
    {
        public Exp obj;
        public string propName;

        public PointExp(Exp obj, string propName)
        {
            this.obj = obj;
            this.propName = propName;
        }

        public object Exec(Ctxt ctxt)
        {
            var objRes = (ObjectInstance)obj.Exec(ctxt);
            if(objRes.Properties.ContainsKey(propName)) 
                return objRes.Properties[propName].Exec(ctxt);
            throw new ArgumentException("Property is not in object");
        }

        public Exp Optimize()
        {
            return this;
        }

        public string DrawTree(int lvl = 0)
        {
            var res = "";
            for (int i = 0; i < lvl; ++i)
            {
                res += "|\t";
            }
            res += "Point" + ":" + '\n';
            res += (obj.DrawTree(lvl + 1));
            for (int i = 0; i < (lvl + 1); ++i)
            {
                res += "|\t";
            }

            res += ("PropertyName: " + propName);
        //    res += "Point";
            return res;
        }
    }
}
