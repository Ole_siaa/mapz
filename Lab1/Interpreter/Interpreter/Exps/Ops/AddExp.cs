﻿using System.Text;
using Interpreter.Exps.Factors;

namespace Interpreter.Exps.Ops
{
    class AddExp : Exp
    {
        public Exp left;
        public Exp right;

        public AddExp(Exp left, Exp right)
        {
            this.left = left;
            this.right = right;
        }

        public object Exec(Ctxt ctxt)
        {
            var leftRes = (int)left.Exec(ctxt);
            var rightRes = (int)right.Exec(ctxt);
            return leftRes + rightRes;
        }

        public Exp Optimize()
        {
            left = left.Optimize();
            right = right.Optimize();
            if (left is IntExp && right is IntExp)
            {
                return new IntExp(((IntExp)left).value + ((IntExp)right).value);

            }

            return this;
        }

        public string DrawTree(int lvl = 0)
        {
            var res = "";
            for (int i = 0; i < lvl; ++i)
            {
                res += "|\t";
            }
            res += "Add" + ":" + '\n';
            res += (left.DrawTree(lvl + 1));
            res += (right.DrawTree(lvl + 1));
       //     res += "Add";
            return res;
        }
    }
}
