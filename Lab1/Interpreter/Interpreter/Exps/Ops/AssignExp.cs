﻿using System.Text;

namespace Interpreter.Exps.Ops
{
    class AssignExp : Exp
    {
        public string varName;
        public Exp exp;

        public AssignExp(string varName, Exp exp)
        {
            this.varName = varName;
            this.exp = exp;
        }

        public object Exec(Ctxt ctxt)
        {
            ctxt.variables[varName] = exp.Exec(ctxt);
            return null;
        }

        public Exp Optimize()
        {
            exp = exp.Optimize();
            return this;
        }

        public string DrawTree(int lvl = 0)
        {
            var res = "";
            for (int i = 0; i < lvl; ++i)
            {
                res += "|\t";
            }
            res += "Assign" + ":" + '\n';
            for (int i = 0; i < (lvl + 1); ++i)
            {
                res += "|\t";
            }
            res += "Variable: " + varName + '\n';
            res += (exp.DrawTree(lvl + 1));
        //    res += "Assign";
            return res;
        }
    }
}
