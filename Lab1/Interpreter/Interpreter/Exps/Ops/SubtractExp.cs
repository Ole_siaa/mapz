﻿using System.Text;
using Interpreter.Exps.Factors;

namespace Interpreter.Exps.Ops
{
    class SubtractExp : Exp
    {
        private Exp left;
        private Exp right;

        public SubtractExp(Exp exp1, Exp exp2)
        {
            left = exp1;
            right = exp2;
        }

        public object Exec(Ctxt ctxt)
        {
            var leftRes = (int)left.Exec(ctxt);
            var rightRes = (int)right.Exec(ctxt);
            return leftRes - rightRes;
        }

        public Exp Optimize()
        {
            left = left.Optimize();
            right = right.Optimize();
            if (left is IntExp && right is IntExp)
            {
                return new IntExp(((IntExp)left).value - ((IntExp)right).value);
            }

            return this;
        }

        public string DrawTree(int lvl = 0)
        {
            var res = "";
            for (int i = 0; i < lvl; ++i)
            {
                res += "|\t";
            }
            res += "Subtract" + ":" + '\n';
            res += (left.DrawTree(lvl + 1));
            res += (right.DrawTree(lvl + 1));
         //   res += "Subtract";
            return res;
        }
    }
}
