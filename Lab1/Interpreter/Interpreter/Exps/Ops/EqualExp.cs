﻿using System.Text;
using Interpreter.Exps.Factors;

namespace Interpreter.Exps.Ops
{
    class EqualExp : Exp
    {
        public Exp left;
        public Exp right;

        public EqualExp(Exp left, Exp right)
        {
            this.left = left;
            this.right = right;
        }

        public object Exec(Ctxt ctxt)
        {
            var leftRes = left.Exec(ctxt);
            var rightRes = right.Exec(ctxt);
            return leftRes.ToString() == rightRes.ToString();
        }

        public Exp Optimize()
        {
            left = left.Optimize();
            right = right.Optimize();
            if (left is StringExp && right is StringExp)
            {
                return new BoolExp(((StringExp)left).value == ((StringExp)right).value);
            }
            if (left is IntExp && right is IntExp)
            {
                return new BoolExp(((IntExp)left).value == ((IntExp)right).value);
            }

            return this;
        }

        public string DrawTree(int lvl = 0)
        {
            var res = "";
            for (int i = 0; i < lvl; ++i)
            {
                res += "|\t";
            }
            res += "Equal" + ":" + '\n';
            res += (left.DrawTree(lvl + 1));
            res += (right.DrawTree(lvl + 1));
        //    res += "Equal";
            return res;
        }
    }
}
