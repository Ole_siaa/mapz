﻿using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Interpreter.Exps
{
    class BlockExp : Exp
    {
        private IList<Exp> stmts;

        public BlockExp(IList<Exp> stmts)
        {
            this.stmts = stmts;
        }

        public object Exec(Ctxt ctxt)
        {
            foreach (var expression in stmts)
            {
                expression.Exec(ctxt);
            }

            return null;
        }

        public Exp Optimize()
        {
            if (stmts == null) return null;
                for (var i = 0; i < stmts.Count; ++i)
                {
                    var optimizedExp = stmts[i].Optimize();
                    if (optimizedExp == null)
                    {
                        stmts.RemoveAt(i);
                        --i;
                    }
                    else
                    {
                        stmts[i] = optimizedExp;
                    }
                }

                return stmts.Count <= 1 ? stmts.FirstOrDefault() : this;

        }

        public string DrawTree(int lvl = 0)
        {
            var res = "";
            for (int i = 0; i < lvl; ++i)
            {
                res += "|\t";
            }
            res += "Block:" + '\n';
            if (stmts != null)
            {
                foreach (var exp in stmts)
                {
                    res += exp.DrawTree(lvl + 1);
                }
            }

            return res;
        }
    }
}
