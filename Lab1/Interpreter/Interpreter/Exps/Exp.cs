﻿namespace Interpreter.Exps
{
    interface Exp
    {
        object Exec(Ctxt ctxt);
        Exp Optimize();
        string DrawTree(int lvl = 0);
    }
}
